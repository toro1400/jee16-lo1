package se.miun.dsv.javaee16.lo1.model;

import java.util.HashSet;
import java.util.Set;

public class User {
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String email;
	private String firstName;
	private String lastName;
	private Set<Comment> comments;
	
	public User() {}
	
	public User(String email, String firstName, String lastName) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.comments = new HashSet<Comment>();
	}
	
	public int getId() {
		return id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public Set<Comment> getComments() {
		return comments;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
	
}
