package se.miun.dsv.javaee16.lo1.model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class Event {
	private static final long serialVersionUID = 1L;
	
	private long id;
	private String title;
	private String city;
	private String content;
	private LocalDateTime startTime;
	private LocalDateTime endTime;
	private LocalDateTime lastUpdate;
	private Set<Comment> comments;
	private Set<User> organizers;
	
	public Event() {}
	
	public Event(String title, String city, String content, LocalDateTime startTime, LocalDateTime endTime, Set<User> organizers) {
		this.title = title;
		this.city = city;
		this.content = content;
		this.startTime = startTime;
		this.endTime = endTime;
		this.organizers = organizers;
		this.comments = new HashSet<Comment>();
	}
	
	public long getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getContent() {
		return content;
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}
	
	public LocalDateTime getEndTime() {
		return endTime;
	}
	
	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}
	
	public Set<User> getOrganizers() {
		return organizers;
	}
	
	public Set<Comment> getComments() {
		return comments;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}
	
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}
	
	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public void setOrganizers(Set<User> organizers) {
		this.organizers = organizers;
	}
	
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}
}
