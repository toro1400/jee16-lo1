package se.miun.dsv.javaee16.lo1.model;

import java.time.LocalDateTime;

public class Comment {
	private static final long serialVersionUID = 1L;
	
	private long id;
	private Event event;
	private String comment;
	private LocalDateTime time;
	private LocalDateTime lastUpdate;
	private User user;
	
	public Comment() {}
	
	public Comment(Event event, String comment, LocalDateTime time, User user) {
		this.event = event;
		this.comment = comment;
		this.time = time;
		this.user = user;
	}
	
	public Comment(Event event, String comment, User user) {
		this(event, comment, LocalDateTime.now(), user);
	}
	
	public long getId() {
		return id;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public String getComment() {
		return comment;
	}
	
	public LocalDateTime getTime() {
		return time;
	}
	
	public LocalDateTime getLastUpdate() {
		return lastUpdate;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public void setTime(LocalDateTime time) {
		this.time = time;
	}
	
	public void setLastUpdate(LocalDateTime lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
}
